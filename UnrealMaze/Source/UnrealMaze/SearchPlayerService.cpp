// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealMaze.h"
#include "SlimeAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "SearchPlayerService.h"

void USearchPlayerService::TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds){
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	auto controller = Cast<ASlimeAIController>(OwnerComp.GetOwner());

	

	//UE_LOG(LogTemp, Warning, TEXT("Searching player"));
	if (controller){

		auto BlackboardComp = controller->GetBlackboardComp();

		int32 key = BlackboardComp->GetKeyID("CanBeSeen");

		if (controller->PlayerCanBeSeen()){
			BlackboardComp->SetValueAsBool(key, true);
		}
		else {
			BlackboardComp->SetValueAsBool(key, false);
		}
	}

	
}



