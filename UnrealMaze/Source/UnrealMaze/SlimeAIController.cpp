// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealMaze.h"
#include "Slime.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "SlimeAIController.h"

ASlimeAIController::ASlimeAIController(const class FObjectInitializer& PCIP):Super(PCIP){
	// create blackboard and behaviour components in the constructor
	BlackboardComp = PCIP.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackBoardComp"));

	BehaviorComp = PCIP.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorComp"));
}

bool ASlimeAIController::PlayerIsOnAttackRange(){
	return PlayerIsOnRange(mAttackRange);
}

bool ASlimeAIController::PlayerIsOnChaseRange(){
	return PlayerIsOnRange(mChaseRange);
}

bool ASlimeAIController::PlayerIsOnRange(float range){

	auto pawn = UGameplayStatics::GetPlayerPawn(this, 0);
	FVector playerPos = pawn->GetActorLocation();

	auto slime = this->GetPawn();
	if (slime){
		FVector dwarfPos = slime->GetActorLocation();
		return FVector::Dist(playerPos, dwarfPos) < range;
	}

	return false;
}

//Returns true if the player can be seen by the slime
bool ASlimeAIController::PlayerCanBeSeen(){
	auto player = UGameplayStatics::GetPlayerPawn(this, 0);
	FVector playerPos = player->GetActorLocation();
	
	auto slime = this->GetPawn();
	FVector slimePos = slime->GetActorLocation();
	if (slime && player){
		FVector diff = playerPos - slimePos;
		float angle = FVector::DotProduct(diff, slime->GetActorRotation().Vector());

		if (LineOfSightTo(player, slimePos + FVector(0.0f, 0.0f, 1.0f)) && angle > 0){
			SetPlayer(player);
			return true;
		}
	}

	return false;
}

void ASlimeAIController::Possess(APawn* InPawn){
	Super::Possess(InPawn);

	ASlime * slime = Cast<ASlime>(InPawn);

	//Start behaviour tree
	auto BT = slime->SlimeBehavior;
	if (slime && BT){
		BlackboardComp->InitializeBlackboard(*BT->BlackboardAsset);

		// Get the enemy blackboard ID, and store it to access that blackboard key later.
		playerKeyId = BlackboardComp->GetKeyID("Enemy");

		BehaviorComp->StartTree(*BT);
	}
}

void ASlimeAIController::SetPlayer(APawn * player){
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsObject(playerKeyId, player);
		SetFocus(player);
	}
}


