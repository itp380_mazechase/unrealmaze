// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Maze.generated.h"


/*Auxiliar maze data structures*/
USTRUCT(BlueprintType)
struct FMazeNode{
	GENERATED_USTRUCT_BODY()


	//0 = free, 1 = blocked, 2 = border, 3 = endpoint, 4 = closedpathend
	UPROPERTY(VisibleAnywhere)
	int32 type;
};

USTRUCT(BlueprintType)
struct FMazeRow
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "2D Array")
		TArray<FMazeNode> Cols;


	//or could be using a simple data type instead of a Data Unit Struct
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="2D Array")
	//TArray<FString> Cols;

	FMazeRow(){}
};

USTRUCT(BlueprintType)
struct FMazeArray
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "2D Array")
	TArray<FMazeRow> Rows;

	FMazeArray(){}
};

UCLASS()
class UNREALMAZE_API AMaze : public AActor
{
	GENERATED_BODY()
public:	
	// Sets default values for this actor's properties
	AMaze();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Create a new random maze
	FMazeArray GenMaze();
	
	//Size Getter
	int32 GetSize(){ return size; };

	//Size Getter
	void SetSize(int32 i){ size = i; };

	//Return a array containing all the path endpoints
	TArray<FVector> SearchEndpoints(FMazeArray maze);

	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
private:
	void MazeGenHelper(int x, int y);
	

protected:

	//Meshs
	UPROPERTY(EditDefaultsOnly, Category = "StaticMesh Components")
	UClass * WallBP;

	UPROPERTY(EditDefaultsOnly, Category = "StaticMesh Components")
	UClass * GroundBP;

	UPROPERTY(EditAnywhere)
	int32 size = 10;

	UPROPERTY(EditAnywhere)
	FMazeArray grid;

};