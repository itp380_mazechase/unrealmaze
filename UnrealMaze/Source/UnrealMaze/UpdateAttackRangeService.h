// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BehaviorTree/BTService.h"
#include "UpdateAttackRangeService.generated.h"

/**
 * 
 */
UCLASS()
class UNREALMAZE_API UUpdateAttackRangeService : public UBTService
{
	GENERATED_BODY()
	
protected:
	virtual void TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds) override;
	
	
};
