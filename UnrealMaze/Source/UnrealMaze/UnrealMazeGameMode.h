// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "Wall.h"
#include "Maze.h"
#include "Target.h"
#include "Slime.h"
#include "UnrealMazeHUD.h"
#include "Sound/SoundCue.h"
#include "UnrealMazeGameMode.generated.h"

UCLASS(minimalapi)
class AUnrealMazeGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AUnrealMazeGameMode();
	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	
	void SpawnWall();
	void ClearWall();
	void TouchTarget();
    void Refresh();
    void GotHit();
    void NextLevel();
    
    int GetLevel(){return level;};
    int GetTime(){return time;};
    int GetScore(){return score;};
    bool IsGameOver(){return gameOver;};

    void Restart();
    
protected:
	UPROPERTY(EditAnywhere, Category = Spawner)
	TSubclassOf<AWall> WallClass;
	UPROPERTY(EditAnywhere, Category = Spawner)
	TSubclassOf<ATarget> TargetClass;
	UPROPERTY(EditAnywhere, Category = Spawner)
	TSubclassOf<AMaze> GeneratorClass;
	UPROPERTY(EditAnywhere, Category = Spawner)
		TSubclassOf<ASlime> EnemyClass;

	UPROPERTY(EditAnywhere, Category = Spawner)
		int32 nEnemies;
    UPROPERTY(EditAnywhere, Category = Spawner)
    int32 initialTime;

    int level;
    int score;
    int time;
    
	//Game Objects
	TArray<AWall*> Walls;
	TArray<ASlime *> Enemies;
	ATarget* target;

    //Sound Effect
    UAudioComponent* PlaySound(USoundCue* Sound);
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    class USoundCue* Desert;
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    class USoundCue* Hit;
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    class USoundCue* Lose;
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    class USoundCue* Next;
    void PlayBGM();
    UAudioComponent* BGMAC;
    FTimerHandle BGMTimerHandle;
    
	//UPROPERTY(EditAnywhere, Category = Spawner)
	class AMaze* MazeGenerator;

	UPROPERTY(EditAnywhere, Category = Spawner)
	int32 mazeSize = 10;

	FTimerHandle LevelTimerHandle;

  
	void LevelTimer();
	void MinusOneSec();

	void GameOver();
	bool gameOver;
    
    
};



