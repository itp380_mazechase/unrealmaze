// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealMaze.h"
#include "SlimeAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "UpdateAttackRangeService.h"

void UUpdateAttackRangeService::TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds){
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	auto controller = Cast<ASlimeAIController>(OwnerComp.GetOwner());

	if (controller){
		bool onRange = controller->PlayerIsOnAttackRange();

		auto BlackboardComp = controller->GetBlackboardComp();

		int32 key = BlackboardComp->GetKeyID("OnAttackRange");

		if (BlackboardComp)
		{
			BlackboardComp->SetValueAsBool(key, onRange);
		}
	}
}



