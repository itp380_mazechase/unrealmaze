// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "SlimeAIController.generated.h"

/**
 * 
 */
UCLASS()
class UNREALMAZE_API ASlimeAIController : public AAIController
{
	GENERATED_BODY()

protected:
	void Possess(APawn* InPaw) override;

	UPROPERTY(transient)
		UBlackboardComponent * BlackboardComp;

	UPROPERTY(transient)
		UBehaviorTreeComponent * BehaviorComp;

	int32 playerKeyId;
public:
	UPROPERTY(EditAnywhere)
		float mAttackRange = 250.0f;

	UPROPERTY(EditAnywhere)
		float mChaseRange = 2000.0f;

	ASlimeAIController(const class FObjectInitializer& PCIP);

	UBlackboardComponent * GetBlackboardComp() { return BlackboardComp; };

	bool PlayerIsOnAttackRange();
	bool PlayerIsOnChaseRange();
	bool PlayerIsOnRange(float range);
	bool PlayerCanBeSeen();

	void SetPlayer(APawn * player);
};
