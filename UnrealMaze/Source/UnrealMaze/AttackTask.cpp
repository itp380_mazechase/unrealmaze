// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealMaze.h"
#include "SlimeAIController.h"
#include "UnrealMazeGameMode.h"
#include "AttackTask.h"

EBTNodeResult::Type UAttackTask::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory){
	
	//TODO: Player get damaged and respawn in a random place

	auto controller = Cast<ASlimeAIController>(OwnerComp.GetOwner());

	UE_LOG(LogTemp, Warning, TEXT("Damaging player"));

	if (controller){
		bool onRange = controller->PlayerIsOnAttackRange();
		if (onRange){
			auto gamemode = Cast<AUnrealMazeGameMode>(GetWorld()->GetAuthGameMode());

			if (gamemode){
				gamemode->GotHit();
			}
		}
	}


	return EBTNodeResult::Succeeded;
}


