// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"

#include "UnrealMazeHUD.generated.h"

UCLASS()
class AUnrealMazeHUD : public AHUD
{
	GENERATED_BODY()

public:
	AUnrealMazeHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;
	/*
    static const FColor		FColorBlack;
    static const FColor		FColorRed;
    static const FColor		FColorYellow;
    static const FColor		FColorBlue;
    static const FColor		FColor_White;
    */

protected:
	/** Crosshair asset pointer */
	//class UTexture2D* CrosshairTex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MazeHUD)
	UFont* HUDFont;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=MazeHUD)
    float DefaultFontScale;
    
    //AUnrealMazeGameMode* gamemode;
	float screenX;
	float screenY;
};

