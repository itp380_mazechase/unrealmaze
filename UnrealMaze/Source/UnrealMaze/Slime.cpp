// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealMaze.h"
#include "Slime.h"
#include "SlimeAIController.h"


// Sets default values
ASlime::ASlime()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Set the AI Controller
	AIControllerClass = ASlimeAIController::StaticClass();
}

// Called when the game starts or when spawned
void ASlime::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASlime::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ASlime::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

