// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealMaze.h"
#include "WallSpawner.h"


// Sets default values
AWallSpawner::AWallSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWallSpawner::BeginPlay()
{
	Super::BeginPlay();
	MazeGenerator = GetWorld()->SpawnActor<AMaze>(GeneratorClass, FVector::ZeroVector, FRotator::ZeroRotator);
	SpawnWall();
}

// Called every frame
void AWallSpawner::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    TouchTarget();
}

void AWallSpawner::SpawnWall(){
	if (MazeGenerator){
		FMazeArray grid = MazeGenerator->GenMaze();
		int32 size = MazeGenerator->GetSize();
        bool playerPosed = false;
		for (int i = 0; i < size; i++){
			for (int j = 0; j < size; j++){
				int32 type = grid.Rows[j].Cols[i].type;
                FVector pos = FVector(i * 100, j * 100, 100);
                if (!playerPosed && type==0){
                    AActor* p = UGameplayStatics::GetPlayerPawn(this, 0);
                    p->SetActorLocation(pos);
                    p->SetActorRotation(FRotator::ZeroRotator);
                }
                if (type == 1 || type ==2 ){
                    AWall* newWall = GetWorld()->SpawnActor<AWall>(WallClass, pos, FRotator::ZeroRotator);
                    Walls.Add(newWall);
                }else if (type==3){
                    target = GetWorld()->SpawnActor<ATarget>(TargetClass, pos, FRotator::ZeroRotator);
                }
			}

		}
	}
}

void AWallSpawner::TouchTarget(){
    if (target){
        FVector targetPos = target->GetActorLocation();
        FVector playerPos = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
        
        float d = FVector::Dist(playerPos,targetPos);
        //UE_LOG(LogTemp,Warning,TEXT("%f"),d);
        if (FVector::Dist(playerPos,targetPos) < 70.0f){
            //UE_LOG(LogTemp, Warning, TEXT("reached target"));
            NextLevel();
            
        }
    }
}


void AWallSpawner::NextLevel(){
    if (Walls.Num() !=0 ){
        for (AWall* iter : Walls){
            iter->Destroy();
        }
    }
    target->Destroy();
    SpawnWall();
}

