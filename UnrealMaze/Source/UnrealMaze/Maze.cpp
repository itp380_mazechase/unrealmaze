// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealMaze.h"
#include "Maze.h"


// Sets default values
AMaze::AMaze()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//GenMaze();
}

// Called when the game starts or when spawned
void AMaze::BeginPlay()
{
	Super::BeginPlay();

	//GenMaze();
}

//Create a new random maze
FMazeArray AMaze::GenMaze(){

	grid = FMazeArray();

	//Initiliaze grid and Close the borders
	for (int i = 0; i < size; i++){
		grid.Rows.Add(FMazeRow());

		if (i == 0 || i == size - 1){ //Top and bottom border
			for (int j = 0; j < size; j++){
				FMazeNode node;
				node.type = 2;

				//This will create a maze exit in the middle of the bottom border, TODO: find a more elegant way
				if (i == size - 1 && j == size / 2){
					node.type = 3;
				}

				grid.Rows[i].Cols.Add(node);
			}
		} else { // right and left border
			for (int j = 0; j < size; j++){
				FMazeNode node;
				if (j == 0 || j == size - 1){
					node.type = 2;
				} else {
					node.type = 1;
				}
				grid.Rows[i].Cols.Add(node);
			}
		}
	}

	//Create the path
	//random start point
	int32 x = size / 2;
	int32 y = size - 2;
	grid.Rows[y].Cols[x].type = 0;
	MazeGenHelper(x, y);

	//Log maze based on grid
	FString out = "\n";
	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			int32 type = grid.Rows[j].Cols[i].type;
			FString n = type == 0 || type == 3? "x" : "o";
			n = type == 4 ? "e" : n;
			out += n;
		}
		out += "\n";
	}

	UE_LOG(LogTemp, Warning, TEXT("%s"), *out);

	return grid;
}

void AMaze::MazeGenHelper(int x, int y){

	int childPaths = 0;

	//Outbound
	if (x < 0 || x >= size || y < 0 || y >= size){
		return;
	}

	//Next tile
	TArray<int32> directions;
	for (int32 i = 0; i < 4; i++){
		directions.Add(i);
	}
	//Randomly select a direction and recurse in that direction
	int maxIndex = 3;
	while (maxIndex > 0){
		int32 index = FMath::RandRange(0, maxIndex);
		int32 dir = directions[index];
		directions.Remove(dir);
		
		maxIndex--;

		int32 tileType;

		switch (dir){
		case 0: //Left
			if (x - 2 < 0)
				continue;
			tileType = grid.Rows[y].Cols[x-2].type;
			if (tileType != 1)
				continue;

			grid.Rows[y].Cols[x - 1].type = 0;
			grid.Rows[y].Cols[x - 2].type = 0;

			UE_LOG(LogTemp, Warning, TEXT("Left"));
			MazeGenHelper(x - 2, y);

			childPaths++;
			break;
		case 1: //Right
			if (x + 2 >= size)
				continue;
			tileType = grid.Rows[y].Cols[x + 2].type;
			if (tileType != 1)
				continue;

			grid.Rows[y].Cols[x + 1].type = 0;
			grid.Rows[y].Cols[x + 2].type = 0;

			UE_LOG(LogTemp, Warning, TEXT("Right"));
			MazeGenHelper(x + 2, y);
			childPaths++;

			break;
		case 2: //Front
			if (y - 2 < 0)
				continue;
			tileType = grid.Rows[y - 2].Cols[x].type;
			if (tileType != 1)
				continue;

			grid.Rows[y-1].Cols[x].type = 0;
			grid.Rows[y-2].Cols[x].type = 0;

			UE_LOG(LogTemp, Warning, TEXT("Front"));
			MazeGenHelper(x, y-2);
			childPaths++;
			break;
		case 3: //Back
			if (y + 2 >= size)
				continue;
			tileType = grid.Rows[y + 2].Cols[x].type;
			if (tileType != 1)
				continue;

			grid.Rows[y + 1].Cols[x].type = 0;
			grid.Rows[y + 2].Cols[x].type = 0;

			UE_LOG(LogTemp, Warning, TEXT("Back"));
			MazeGenHelper(x, y + 2);
			childPaths++;
			break;

		}
	}

	//Pathend
	if (childPaths == 0){
		grid.Rows[y].Cols[x].type = 4;
	}
}

TArray<FVector> AMaze::SearchEndpoints(FMazeArray grid){
	TArray<FVector> out;

	int size = grid.Rows.Num();
	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			int32 type = grid.Rows[j].Cols[i].type;
			if (type == 4){
				FVector vect;
				vect.X = i;
				vect.Y = j;
				out.Add(vect);
			}
		}
	}

	//Shuffle
	for (int i = 0; i < out.Num(); i++){
		FVector temp = out[i];
		int nIndex = FMath::RandRange(0, out.Num()-1);
		out[i] = out[nIndex];
		out[nIndex] = temp;
	}


	return out;
}

// Called every frame
void AMaze::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

