// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "UnrealMaze.h"
#include "UnrealMazeHUD.h"
#include "UnrealMazeGameMode.h"
#include "Engine/Canvas.h"
#include "TextureResource.h"
#include "CanvasItem.h"

// Colors
/*
const FColor AUnrealMazeHUD::FColorBlack 		= FColor(0,0,0,255);
const FColor AUnrealMazeHUD::FColorRed 			= FColor(255,0,0,255);
const FColor AUnrealMazeHUD::FColorYellow 		= FColor(255,255,0,255);
const FColor AUnrealMazeHUD::FColorBlue			= FColor(0,0,255,255);
const FColor AUnrealMazeHUD::FColor_White		= FColor(255,255,255,255);
*/
AUnrealMazeHUD::AUnrealMazeHUD()
{
    //HUDFont = CreateDefaultSubobject<UFont>(TEXT("HUDFont"));
    
	static ConstructorHelpers::FObjectFinder<UFont>HUDFontOb(TEXT("/Engine/EngineFonts/RobotoDistanceField"));
	HUDFont = HUDFontOb.Object;

}


void AUnrealMazeHUD::DrawHUD()
{
	Super::DrawHUD();
    auto gamemode = Cast<AUnrealMazeGameMode>(GetWorld()->GetAuthGameMode());
	screenX = Canvas->SizeX;
	screenY = Canvas->SizeY;    
    
    
    
	DrawText(FString::Printf(TEXT("Level: %d"),gamemode->GetLevel()), FColor::White, 50, 30, HUDFont);
	DrawText(FString::Printf(TEXT("Score: %d"),gamemode->GetScore()), FColor::White, 50, 60, HUDFont);
	DrawText(FString::Printf(TEXT("Time: %d"),gamemode->GetTime()), FColor::White, screenX*0.5f, 30, HUDFont);
	if (gamemode->IsGameOver()){
		DrawText(FString::Printf(TEXT("Game Over")), FColor::White, screenX*0.5f, screenY*0.5f, HUDFont);
        DrawText(FString::Printf(TEXT("Press ENTER to restart")), FColor::White, screenX*0.5f-60, screenY*0.5f+30, HUDFont);
    }
}

