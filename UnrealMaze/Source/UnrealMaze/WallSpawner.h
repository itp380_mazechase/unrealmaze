// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Wall.h"
#include "Maze.h"
#include "Target.h"
#include "WallSpawner.generated.h"


UCLASS()
class UNREALMAZE_API AWallSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWallSpawner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

    void SpawnWall();
    void ClearWall();
    
    void TouchTarget();
    void NextLevel();
    
protected:
	UPROPERTY(EditAnywhere, Category = Spawner)
	TSubclassOf<AWall> WallClass;
    UPROPERTY(EditAnywhere, Category = Spawner)
    TSubclassOf<ATarget> TargetClass;
	UPROPERTY(EditAnywhere, Category = Spawner)
	TSubclassOf<AMaze> GeneratorClass;
    
    //Game Objects
	TArray<AWall*> Walls;
    ATarget* target;
    
	//UPROPERTY(EditAnywhere, Category = Spawner)
	class AMaze* MazeGenerator;

};
