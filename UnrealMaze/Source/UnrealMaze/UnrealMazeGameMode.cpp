// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "UnrealMaze.h"
#include "UnrealMazeGameMode.h"
#include "UnrealMazeHUD.h"
#include "UnrealMazeCharacter.h"

AUnrealMazeGameMode::AUnrealMazeGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AUnrealMazeHUD::StaticClass();

}

void AUnrealMazeGameMode::BeginPlay()
{
	Super::BeginPlay();
    level = 1;
    score = 0;
    time = initialTime;
	gameOver = false;
	
	MazeGenerator = GetWorld()->SpawnActor<AMaze>(GeneratorClass, FVector::ZeroVector, FRotator::ZeroRotator);
	MazeGenerator->SetSize(mazeSize);
	
	SpawnWall();
	LevelTimer();
    
    PlayBGM();
  
}

void AUnrealMazeGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TouchTarget();
}

void AUnrealMazeGameMode::SpawnWall(){
	if (MazeGenerator){
		int32 size = MazeGenerator->GetSize();
		size += 2 * (level - 1);
		MazeGenerator->SetSize(size);
		FMazeArray grid = MazeGenerator->GenMaze();
		
		bool playerPosed = false;
		for (int i = 0; i < size; i++){
			for (int j = 0; j < size; j++){
				int32 type = grid.Rows[j].Cols[i].type;
				FVector pos = FVector(i * 100, j * 100, 100);
				if (type == 1 || type == 2){
					AWall* newWall = GetWorld()->SpawnActor<AWall>(WallClass, pos, FRotator::ZeroRotator);
					Walls.Add(newWall);
				}
				else if (type == 3){
					target = GetWorld()->SpawnActor<ATarget>(TargetClass, pos, FRotator::ZeroRotator);
				}
			}

		}


		/*Spawn player and enemies*/
		TArray<FVector> endpoints = MazeGenerator->SearchEndpoints(grid);
		for (int i = 0; i < nEnemies + 1; i++){
			if (i > endpoints.Num() - 1)
				break;
			
			FVector pos = FVector(endpoints[i].X * 100, endpoints[i].Y * 100, 100);

			if (i == 0){
				AActor* p = UGameplayStatics::GetPlayerPawn(this, 0);
				p->SetActorLocation(pos);
				p->SetActorRotation(FRotator::ZeroRotator);
			}
			else {
				ASlime* newEnemy = GetWorld()->SpawnActor<ASlime>(EnemyClass, pos, FRotator::ZeroRotator);
				if (newEnemy){
					newEnemy->SpawnDefaultController();
					Enemies.Add(newEnemy);
				}
				
			}
		}
	}
}

void AUnrealMazeGameMode::TouchTarget(){
	if (target && !gameOver){
		FVector targetPos = target->GetActorLocation();
		FVector playerPos = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();

		float d = FVector::Dist(playerPos, targetPos);
		//UE_LOG(LogTemp,Warning,TEXT("%f"),d);
		if (FVector::Dist(playerPos, targetPos) < 70.0f){
			//UE_LOG(LogTemp, Warning, TEXT("reached target"));
			NextLevel();
			
		}
	}
}

void AUnrealMazeGameMode::Refresh(){
    if (Walls.Num() != 0){
        for (AWall* iter : Walls){
            iter->Destroy();
        }
        Walls.Empty();
    }
    
    if (Enemies.Num() != 0){
        for (AActor* iter : Enemies){
            iter->Destroy();
        }
        Enemies.Empty();
    }
    
    
    target->Destroy();
    SpawnWall();
    
}

void AUnrealMazeGameMode::GotHit(){
    if (level>=1){
        level--;
    }
    if (level < 1){
        GameOver();
    }else{
        PlaySound(Hit);
        Refresh();
        score = score - time * 5;
        if (score < 0){
            score = 0;
        }
    }
}

void AUnrealMazeGameMode::NextLevel(){
    PlaySound(Next);
    Refresh();
    level++;
    time = time + time/2;
    score = score + time * 10;
	time += level * 5;
}


void AUnrealMazeGameMode::LevelTimer(){
	GetWorldTimerManager().SetTimer(LevelTimerHandle, this, &AUnrealMazeGameMode::MinusOneSec,1.0f, true);
}

void AUnrealMazeGameMode::MinusOneSec(){
    time--;
	if (time <= 0){
		
		GameOver();
	}

}

void AUnrealMazeGameMode::GameOver(){
	gameOver = true;
    BGMAC->Stop();
    GetWorldTimerManager().ClearTimer(LevelTimerHandle);
    GetWorldTimerManager().ClearTimer(BGMTimerHandle);
    PlaySound(Lose);
}


UAudioComponent* AUnrealMazeGameMode::PlaySound(USoundCue* Sound)
{
    UAudioComponent* AC = NULL;
    if (Sound)
    {
        AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
    }
    return AC;
}

void AUnrealMazeGameMode::PlayBGM(){
    BGMAC = PlaySound(Desert);
    GetWorldTimerManager().SetTimer(BGMTimerHandle, this, &AUnrealMazeGameMode::PlayBGM,32.0f, true);
}

void AUnrealMazeGameMode::Restart(){
    if (gameOver){
        gameOver = false;
        level = 1;
        score = 0;
        time = initialTime;
        Refresh();
        LevelTimer();
        PlayBGM();
    }
}



