// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealMaze.h"
#include "SlimeAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "UpdateChaseRangeService.h"

void UUpdateChaseRangeService::TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds){
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	auto controller = Cast<ASlimeAIController>(OwnerComp.GetOwner());

	if (controller){

		
		bool onRange = controller->PlayerIsOnChaseRange();

		auto BlackboardComp = controller->GetBlackboardComp();

		int32 key = BlackboardComp->GetKeyID("OnChaseRange");

		if (BlackboardComp)
		{
			//UE_LOG(LogTemp, Warning, TEXT("Updating Chase Range"));
			BlackboardComp->SetValueAsBool(key, onRange);
		}
	}


}


